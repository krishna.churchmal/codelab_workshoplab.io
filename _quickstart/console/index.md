---
layout: default
title: Console
# parent: Elysium Quick Start
has_toc: false
has_children: false
nav_order: 3
redirect_from: 
  - /opensearch/install/console/
---

# Console

Elysium Console is split in to 2 sections, Menu and Landing section based on the component you choose in the menu the respective component would be available to use in the landing page 

The Menu is divided in to 5 sections. 

1. Log Management  

2. Observability  

3. Security

4. Configuration  

5. User Management 