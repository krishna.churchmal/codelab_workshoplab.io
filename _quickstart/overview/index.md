---
layout: default
title: Overview
# parent: Elysium Quick Start
nav_order: 2
has_toc: false
has_children: false
redirect_from: 
  - /opensearch/install/quickstart/
---


# Overview

Elysium Analytics is a modern purpose-built logs, metrics and traces data cloud that combines disparate data points across sources with a connected data model, transforming raw data into actionable insights, making lives easier for your SOC teams and improving 10X productivity 

 

This guide will walk you through the steps to setup data collection from AWS, OKTA and NETSKOPE sources and load to databricks and quickly perform analytics on top of it