---
layout: default
title: Login
nav_order: 12
parent: Getting Started
# grand_parent: Log Analytics
# has_children: true
redirect_from:
  - /documentation/log-analytics/opensearch-dashboard/exploring-data
---


# Login

Login to https://databricks.elysiumanalytics.ai/ using the provided credentials :


![dfvd]({{site.baseurl}}/images/Picture1.png)