---
layout: default
title: Configurations
nav_order: 14
parent: Getting Started
# grand_parent: Log Analytics
# has_children: true
redirect_from:
  - /documentation/log-analytics/opensearch-dashboard/exploring-data
---


# Configurations

**Discover**  in OpenSearch Dashboards helps you extract insights and get value out of data assets across your organization. Discover enables you to:

