---
layout: default
title: Getting Started
# parent: Elysium Quick Start
has_children: true
nav_order: 4
redirect_from: 
  - /opensearch/install/get-started/
---

# Getting Started

Get started using OpenSearch and OpenSearch Dashboards by deploying your containers with [Docker](https://www.docker.com/). Before proceeding, you need to [get Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://github.com/docker/compose) installed on your local machine. 