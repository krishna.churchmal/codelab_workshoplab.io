---
layout: default
title: Log Management
parent: Exploring Data
grand_parent: Getting Started
nav_order: 13
---


# Log Management

You can change the time range to display dashboard data over minutes, hours, days, weeks, months, or years.