---
layout: default
title: Exploring Data
nav_order: 15
parent: Getting Started
# grand_parent: Log Analytics
has_children: true
redirect_from:
  - /documentation/log-analytics/opensearch-dashboard/exploring-data
---


# Exploring data

**Discover**  in OpenSearch Dashboards helps you extract insights and get value out of data assets across your organization. Discover enables you to:

