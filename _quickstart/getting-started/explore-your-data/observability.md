---
layout: default
title: Observability
parent: Exploring Data
grand_parent: Getting Started
nav_order: 13
---


# Observability

You can change the time range to display dashboard data over minutes, hours, days, weeks, months, or years.