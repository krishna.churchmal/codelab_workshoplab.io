---
layout: default
title: Data Source Onboarding
nav_order: 13
parent: Getting Started
# grand_parent: Log Analytics
# has_children: true
redirect_from:
  - /documentation/log-analytics/opensearch-dashboard/exploring-data
---


# Data Source Onboarding

i.	**Setup Data connection**:
Choose admin in the Menu
 
![dfvd]({{site.baseurl}}/images/Picture2.png)

In the **Data Connection** tab, Click on **“Add New Connection”** and specify the respective database connection values

![dfvd]({{site.baseurl}}/images/Picture3.png)
 
Once Saved, the connection must be in Active State 
 
![dfvd]({{site.baseurl}}/images/Picture4.png) 

ii.	**Integrations**:

Choose **Integrations** from the Left Side Menu

![dfvd]({{site.baseurl}}/images/Picture5.png) 
 
Use AWS Service Logs (VPC), OKTA or Netskope connectors.
 
![dfvd]({{site.baseurl}}/images/Picture6.png) 

Setup data collection for 
1.	**AWS VPC**:

Click on **AWS Services Logs**

![dfvd]({{site.baseurl}}/images/Picture7.png) 

Click on **Add New Feed** button on the right corner of the AWS service logs page.

![dfvd]({{site.baseurl}}/images/Picture8.png)  

  1.	Feed Name: keep the name of your choice and relevant to easily identity e.g “aws_vpc_logs”
  2.	Account ID: AWS Account ID where the VPC logs would be sourced from
  3.	Role: Provide the AWS Role Name which has the following permissions or attached as policy

![dfvd]({{site.baseurl}}/images/Picture10.png)

![dfvd]({{site.baseurl}}/images/Picture9.png)

Copy the ARN and add it to your role policy in the respective AWS account like below

![dfvd]({{site.baseurl}}/images/Picture11.png)

![dfvd]({{site.baseurl}}/images/Picture12.png)

![dfvd]({{site.baseurl}}/images/Pic13.png)

Choose the **Amazon Virtual Private Cloud** Service and click Next

![dfvd]({{site.baseurl}}/images/Picture13.png)

The connector gets created, status would show as “Pending” while the Cloud Formation setup gets completed in AWS to collect the logs

![dfvd]({{site.baseurl}}/images/Picture14.png)
 
You can verify the same in the AWS cloud formation console, which would have the stack name suffixed with the connector ID (It is 34 in this case)

![dfvd]({{site.baseurl}}/images/Picture15.png)
 
Once it is setup the connector status would become “Active”

![dfvd]({{site.baseurl}}/images/Picture16.png)
 
Once it is active, A Notebook gets created in databricks with the same name prefixed with EA_VPC_Ingestion

![dfvd]({{site.baseurl}}/images/Picture17.png)
 
Data gets loaded to hive_metastore.connector.bronze_aws_vpc_flow based on the scheduler that is setup in databricks
 
![dfvd]({{site.baseurl}}/images/Picture18.png)
 
2.	**OKTA**:

![dfvd]({{site.baseurl}}/images/Picture19.png)

Status would show as active

 
OKTA Logs data gets loaded to hive_metastore.connector.bronze_aws_okta_logs based on the scheduler setup in databricks

3.	**NetSkope**:

![dfvd]({{site.baseurl}}/images/Picture122.png)

Status would immediately show as active

 
Netskope Logs data gets loaded to hive_metastore.connector.bronze_aws_okta_logs based on the scheduler setup in databricks
