---
layout: default
title: Console
parent: Elysium Quick Start
nav_order: 3
redirect_from: 
  - /opensearch/install/console/
---

# Quickstart

Get started using OpenSearch and OpenSearch Dashboards by deploying your containers with [Docker](https://www.docker.com/). Before proceeding, you need to [get Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://github.com/docker/compose) installed on your local machine. 